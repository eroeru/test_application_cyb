
Feature: Add Access Rights to a Service
  As a Service Administrator
  I want to add access rights to a Service
  For select Service Subjects

  Scenario: Go to Access Rights
    Given I am a Service Administrator
    When I select the Services through the Configuration menu
    And click Access Rights
    Then a window opens with an access rights' table

  #Runs only after a successful first scenario run:
  Scenario: Give Subjects Access Rights
    Given the window opens with an access rights' table
    And the chosen Subjects are not in the access rights' list of this service
    When I give access rights to chosen Subjects from the table
    Then the chosen Subjects' access rights records are added to the system configuration
    And an audit log record for the event is created

  #Runs only after a successful first scenario run:
  Scenario: Try Giving Access Rights to Subjects who Already Have Access Rights
    Given the window opens with an access rights' table
    And the chosen Subjects are in the access rights list of this service
    When I give access rights to chosen Subjects from the table
    Then the system configuration is not changed
    And an audit log record for the event is not created