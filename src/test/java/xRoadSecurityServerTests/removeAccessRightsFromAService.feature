
Feature: Remove Access Rights from a Service
  As a Service Administrator
  I want to remove access rights from a Service
  For select Service Subjects

  Scenario: Go to Access Rights
    Given I am a Service Administrator
    When I select the Services through the Configuration menu
    And click Access Rights
    Then a window opens with an access rights' table

  #Runs only after a successful first scenario run:
  Scenario: Remove Access Rights from Subjects
    Given the window opens with an access rights' table
    When I remove access rights from chosen Subjects from the table
    Then the chosen Subjects' access rights records are deleted from the system configuration
    And the system logs the event "Remove access rights from service" to the audit log

