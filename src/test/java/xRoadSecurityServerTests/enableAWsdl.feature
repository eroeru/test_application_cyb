
Feature: Enable a WSDL
  As a Service Administrator
  I need to enable WSDL services
  So that select Service Clients can use services included in the WSDL

  Scenario: Enable a WSDL
    Given I am a Service Administrator
    And the WSDL is disabled
    When I select the WSDL through the Configuration menu
    And click Enable
    Then the system activates the WSDL for service clients
    And logs the event “Enable WSDL” to the audit log

  #Runs only after a successful first scenario run:
  Scenario: Use enabled WSDL
    Given I am a Service Client
    When I try to use services included in the WSDL
    Then they will be available for use

  Scenario: Enable a WSDL without proper access rights
    Given I am not a Service Administrator
    And the WSDL is disabled
    When I try to select the WSDL through the Configuration menu
    And click Enable
    Then the WSDL is still disabled

  Scenario: Try to enable an enabled WSDL
    Given I am a Service Administrator
    And the WSDL is enabled
    When I try to select the WSDL through the Configuration menu
    And click Enable
    Then the WSDL is still enabled

