
Feature: Disable a WSDL
  As a Service Administrator
  I need to disable WSDL services
  So that select Service Clients will be unable to use services included in the WSDL

  Scenario: Click to disable a WSDL
    Given I am a Service Administrator
    And the WSDL is enabled
    When I select the WSDL through the Configuration menu
    And click Disable
    Then the system asks for a clients' notification message

  Scenario: Enter error message
    Given I am a Service Administrator
    And the system asks for a clients' notification message
    When entering the message
    Then the system parses user input
    And disables the services described in the WSDL
    And logs the event "Disable WSDL" to the audit log

  #Runs only after a successful second scenario run:
  Scenario: Try to access disabled services
    Given I am a Service Client
    When trying to access the disabled services
    Then I will get the error message as a response

  #Runs only after a successful first scenario run:
  Scenario: Not entering error message
    Given the system asks for the clients' notification message
    When I cancel writing the error message
    Then the system displays parse error message
    And logs the event "Disable WSDL failed" to the audit log

  Scenario: Disable a WSDL without proper access rights
    Given I am not a Service Administrator
    And the WSDL is enabled
    When I try to select the WSDL through the Configuration menu
    And click Disable
    Then the WSDL is still enabled

  Scenario: Try to disable a disabled WSDL
    Given I am a Service Administrator
    And the WSDL is disabled
    When I try to select the WSDL through the Configuration menu
    And click Disable
    Then the WSDL is still disabled

