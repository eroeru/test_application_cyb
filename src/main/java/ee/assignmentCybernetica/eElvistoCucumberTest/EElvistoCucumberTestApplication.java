package ee.assignmentCybernetica.eElvistoCucumberTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EElvistoCucumberTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(EElvistoCucumberTestApplication.class, args);
	}

}
